#!/bin/bash

HOST_NGINX=$(terraform output -raw external_ip_address_nginx)
HOST_APP=$(terraform output -raw external_ip_address_app)
HOST_APP2=$(terraform output -raw external_ip_address_app2)

# nginx
ssh dany@$HOST_NGINX << EOF
sudo apt-get install git --yes
sudo apt-get install docker --yes
sudo apt-get install docker-compose --yes
sudo docker stop
sudo dockerd &
git clone https://gitlab.com/SunForLife/hse-cloud-2020-hw-5-storage.git
cd hse-cloud-2020-hw-5-storage
sudo docker-compose up --build --force-recreate --detach nginx
EOF

# app
ssh dany@$HOST_APP << EOF
sudo apt-get install git --yes
sudo apt-get install docker --yes
sudo apt-get install docker-compose --yes
sudo docker stop
sudo dockerd &
git clone https://gitlab.com/SunForLife/hse-cloud-2020-hw-5-storage.git
cd hse-cloud-2020-hw-5-storage
sudo docker-compose up --build --force-recreate --detach service
EOF

# app2
ssh dany@$HOST_APP2 << EOF
sudo apt-get install git --yes
sudo apt-get install docker --yes
sudo apt-get install docker-compose --yes
sudo docker stop
sudo dockerd &
git clone https://gitlab.com/SunForLife/hse-cloud-2020-hw-5-storage.git
cd hse-cloud-2020-hw-5-storage
sudo docker-compose up --build --force-recreate --detach service
EOF

echo $HOST_NGINX
